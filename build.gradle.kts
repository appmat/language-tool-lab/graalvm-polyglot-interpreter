import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val koin_version: String by project
val ktor_version: String by project
val kotest_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val prometeus_version: String by project
val postgresql_version: String by project
val hikari_version: String by project
val exposed_version: String by project

val jep_version: String by project
val ki_shell: String by project
val scala_flink_version: String by project
val jackson_version: String by project
val kotlin_scripting_version: String by project
val graalvm_js: String by project

plugins {
    application
    kotlin("jvm") version "1.5.20"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.5.20"
}

group = "org.appmat.polyglot"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.netty.EngineMain")
    applicationDefaultJvmArgs = listOf(
        "-Djava.library.path=/Library/Frameworks/Python.framework/Versions/3.9/lib/python3.9/site-packages/jep/"
    )
}

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    // Ktor
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-serialization:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("top.jfunc.json:Json-Gson:1.0")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")

    // Metrics
    implementation("io.ktor:ktor-metrics-micrometer:$ktor_version")
    implementation("io.micrometer:micrometer-registry-prometheus:$prometeus_version")
    implementation("io.ktor:ktor-metrics:$ktor_version")

    // JWT
    implementation("io.ktor:ktor-auth:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")

    // Health check
    implementation("com.github.zensum:ktor-health-check:011a5a8")

    // Koin
    implementation("io.insert-koin:koin-ktor:$koin_version")
    implementation("io.insert-koin:koin-logger-slf4j:$koin_version")
    testImplementation("io.insert-koin:koin-test-junit5:$koin_version")

    // Kotest
    testImplementation("io.kotest:kotest-runner-junit5:$kotest_version")
    testImplementation("io.kotest:kotest-assertions-core:$kotest_version")
    testImplementation("io.kotest:kotest-property:$kotest_version")
    testImplementation("io.kotest:kotest-property-datetime:$kotest_version")
    testImplementation("io.kotest.extensions:kotest-extensions-koin:1.0.0")
    testImplementation("io.kotest.extensions:kotest-assertions-ktor:1.0.3")

    // Exposed
    implementation("org.jetbrains.exposed:exposed-core:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-dao:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposed_version")

    // Database
    implementation("org.postgresql:postgresql:$postgresql_version")
    implementation("com.zaxxer:HikariCP:$hikari_version")

    // -----------------------------------------------------------------------------------------------
    // Core
    // Python Jep
    implementation("black.ninia:jep:$jep_version")

    // kotlin lib & dataframe
    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
    implementation("org.jetbrains.kotlinx:dataframe:0.8.0-rc-7")

    // Kotlin KI Shell
    implementation("org.jetbrains.kotlinx:ki-shell:$ki_shell")

    // Scala Shell
    compileOnly("org.apache.flink:flink-core:$scala_flink_version")
    compileOnly("org.apache.flink:flink-streaming-scala_2.12:$scala_flink_version")
    implementation("org.apache.flink:flink-scala_2.12:$scala_flink_version")

    // JSON Serializer
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jackson_version")

    // JS GraalVM
    implementation("org.graalvm.js:js:$graalvm_js")
}

/**
 * stage task is executed in buildpack jvm pipeline
 * @see <a href="https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku">https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku</a>
 */
tasks.register("stage") {
    dependsOn("installDist")
    /**
     * generates the Procfile that specifies a command to run the application
     */
    doLast {
        val relativeInstallationPath = tasks.installDist.get().destinationDir.relativeTo(project.projectDir)
        File("Procfile").writeText(
            """
                web: ./$relativeInstallationPath/bin/${project.name}
            """.trimIndent()
        )
    }
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.freeCompilerArgs += "-Xopt-in=io.ktor.locations.KtorExperimentalLocationsAPI"
}