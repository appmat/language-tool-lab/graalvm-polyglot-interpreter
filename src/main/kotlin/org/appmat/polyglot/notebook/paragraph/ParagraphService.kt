package org.appmat.polyglot.notebook.paragraph

interface ParagraphService {
    fun createParagraph(paragraph: Paragraph): Int
    fun deleteParagraph(notebookId: Int, paragraphId: Int): Int
}