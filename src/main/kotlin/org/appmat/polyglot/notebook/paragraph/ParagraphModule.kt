package org.appmat.polyglot.notebook.paragraph

import org.appmat.polyglot.notebook.paragraph.repository.ParagraphRepository
import org.koin.dsl.module

val paragraphModule = module {
    single { ParagraphRepository() }
    single<ParagraphService>{ ParagraphServiceImpl() }
}