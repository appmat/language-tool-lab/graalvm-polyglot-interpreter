package org.appmat.polyglot.notebook.paragraph.repository;

import org.appmat.polyglot.notebook.repository.NotebookTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object ParagraphTable: Table("paragraph") {
    val id = integer("id").autoIncrement()
    val paragraphId = integer("paragraph_id").uniqueIndex()
    val notebookId = reference(
        "notebook_id",
        NotebookTable.notebookId,
        onDelete = ReferenceOption.SET_NULL,
        onUpdate = ReferenceOption.CASCADE
    )
    val language = varchar("language", 50)
    val code = text("code")
    val output = text("output")

    override val primaryKey = PrimaryKey(paragraphId)
}