package org.appmat.polyglot.notebook.paragraph

data class Paragraph(
    var id: Int,
    var notebookId: Int,
    var language: String = "",
    var code: String = "",
    var output: String = ""
)