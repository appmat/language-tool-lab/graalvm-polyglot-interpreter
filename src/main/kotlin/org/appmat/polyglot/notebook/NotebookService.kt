package org.appmat.polyglot.notebook

import org.appmat.polyglot.notebook.kernel.PolyglotContext

interface NotebookService {
    fun getAllNotebooks(userId: String): List<Notebook>
    fun getNotebookById(userId: String, notebookId: String): Notebook?
    fun updateNotebook(notebook: Notebook)
    fun deleteNotebookById(userId: String, notebookId: String): String
    fun createNotebook(userId: String, title: String): Notebook

    fun runParagraph(userId: String, notebookId: String, lang: String, code: String): Map<PolyglotContext?, String>

    fun startKernel(userId: String, notebookId: String): String
    fun killKernel(userId: String, notebookId: String): String
}