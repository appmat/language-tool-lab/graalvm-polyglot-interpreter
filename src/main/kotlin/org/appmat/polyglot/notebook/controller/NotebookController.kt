package org.appmat.polyglot.notebook.controller

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.locations.patch
import io.ktor.locations.post
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.appmat.polyglot.notebook.Notebook
import org.appmat.polyglot.notebook.NotebookService
import org.appmat.polyglot.notebook.controller.NotebookMapper.toContextDto
import org.appmat.polyglot.notebook.controller.NotebookMapper.toNotebookDto
import org.appmat.polyglot.notebook.controller.NotebookMapper.toNotebookWithIdAndTitleDto
import org.appmat.polyglot.notebook.controller.NotebookMapper.toNotebooksDto
import org.appmat.polyglot.notebook.controller.NotebookUtils.receiveAuthToken
import org.appmat.polyglot.notebook.controller.NotebookUtils.receiveNotebookAndParagraphIds
import org.appmat.polyglot.notebook.controller.NotebookUtils.receiveNotebookId
import org.appmat.polyglot.notebook.dto.CreateNotebookRequest
import org.appmat.polyglot.notebook.dto.NotebookDto
import org.appmat.polyglot.notebook.dto.ParagraphRequest
import org.appmat.polyglot.notebook.paragraph.Paragraph
import org.appmat.polyglot.notebook.paragraph.ParagraphService
import org.appmat.polyglot.plugins.routing.KtorController
import org.appmat.polyglot.user.UserService

class NotebookController(
    private val notebookService: NotebookService,
    private val userService: UserService,
    private val paragraphService: ParagraphService,
) : KtorController {

    override val routing: Routing.() -> Unit = {
        authenticate("auth-jwt") {
            get<Api.NotebookApi.All> {
                val principal = call.principal<JWTPrincipal>()
                val token = principal!!.payload.getClaim("token").asString()
                val userId: String = userService.getUserIdByToken(token)!!
                var notebooks = notebookService.getAllNotebooks(userId)
                val notebooksIds = arrayListOf<Int>()
                notebooks.map { notebooksIds.add(it.id) }
                notebooks = notebooks.filter { notebooksIds.contains(it.id) }
                call.respond(HttpStatusCode.OK, notebooks.toNotebooksDto())
            }
        }

        authenticate("auth-jwt") {
            post<Api.NotebookApi.Create> {
                val principal = call.principal<JWTPrincipal>()
                val token = principal!!.payload.getClaim("token").asString()
                val resp = call.receive<CreateNotebookRequest>()
                val userId: String = userService.getUserIdByToken(token)!!
                val notebook = notebookService.createNotebook(userId, resp.title)
                call.respond(HttpStatusCode.OK, notebook.toNotebookWithIdAndTitleDto())
            }
        }

        authenticate("auth-jwt") {
            delete<Api.NotebookApi.Delete> {
                val principal = call.principal<JWTPrincipal>()
                val token = principal!!.payload.getClaim("token").asString()
                val notebookId = call.receiveNotebookId()
                val userId: String = userService.getUserIdByToken(token)!!
                val response = notebookService.deleteNotebookById(userId, notebookId)
                call.respond(HttpStatusCode.OK, "notebook $response deleted")
            }
        }

        authenticate("auth-jwt") {
            get<Api.NotebookApi.Open> {
                val principal = call.principal<JWTPrincipal>()
                val token = principal!!.payload.getClaim("token").asString()
                val notebookId = call.receiveNotebookId()

                val userId: String = userService.getUserIdByToken(token)!!
                val notebook = notebookService.getNotebookById(userId, notebookId)
                if (notebook != null)
                    call.respond(HttpStatusCode.OK, notebook.toNotebookDto())
                else
                    call.respond(HttpStatusCode.InternalServerError, "")
            }

            authenticate("auth-jwt") {
                patch<Api.NotebookApi.Update> {
                    val notebookDto = call.receive<NotebookDto>()
                    val paragraphs = arrayListOf<Paragraph>()
                    notebookDto.paragraphs.forEach {
                        paragraphs.add(
                            Paragraph(
                                it.id, it.notebookId,
                                it.language, it.code, it.output
                            )
                        )
                    }
                    val notebook = Notebook(notebookDto.id, notebookDto.title, notebookDto.date, paragraphs)
                    notebookService.updateNotebook(notebook)
                    call.respond(HttpStatusCode.OK, "notebook ${notebook.id} updated")
                }
            }

            authenticate("auth-jwt") {
                post<Api.NotebookApi.Paragraph.Create> {
                    val principal = call.principal<JWTPrincipal>()
                    val token = principal!!.payload.getClaim("token").asString()
                    val notebookId = call.receiveNotebookId()
                    val userId: String = userService.getUserIdByToken(token)!!
                    val paragraph = Paragraph(0, notebookId.toInt())
                    call.respond(HttpStatusCode.OK, paragraphService.createParagraph(paragraph))
                }
            }

            authenticate("auth-jwt") {
                delete<Api.NotebookApi.Paragraph.Delete> {
                    val principal = call.principal<JWTPrincipal>()
                    val token = principal!!.payload.getClaim("token").asString()
                    val (notebookId, paragraphId) = call.receiveNotebookAndParagraphIds()

                    val userId: String = userService.getUserIdByToken(token)!!
                    paragraphService.deleteParagraph(notebookId.toInt(), paragraphId.toInt())
                    call.respond(HttpStatusCode.OK, "paragraph $paragraphId deleted")
                }
            }

            authenticate("auth-jwt") {
                post<Api.NotebookApi.Paragraph.Run> {
                    val (lang, code) = call.receive<ParagraphRequest>()
                    val principal = call.principal<JWTPrincipal>()
                    val token = principal!!.payload.getClaim("token").asString()
                    val (notebookId, paragraphId) = call.receiveNotebookAndParagraphIds()
                    val userId: String = userService.getUserIdByToken(token)!!
                    val contextWithErrorMap = notebookService.runParagraph(userId, notebookId, lang, code)
                    val err = contextWithErrorMap.values.first()
                    val context = contextWithErrorMap.keys.first()
                    if (err == "")
                        call.respond(HttpStatusCode.OK, context!!.toContextDto())
                    else
                        call.respond(HttpStatusCode.InternalServerError, err)
                }
            }

            post<Api.NotebookApi.Kernel.Start> {
                val token = call.receiveAuthToken() ?: ""
                val userId: String = userService.getUserIdByToken(token)!!
                val notebookId = call.receiveNotebookId()
                val error = notebookService.startKernel(userId, notebookId)
                if (error == "")
                    call.respond(HttpStatusCode.OK, "")
                else
                    call.respond(HttpStatusCode.InternalServerError, error)
            }

            post<Api.NotebookApi.Kernel.Kill> {
                val token = call.receiveAuthToken() ?: ""
                val userId: String = userService.getUserIdByToken(token)!!
                val notebookId = call.receiveNotebookId()
                val error = notebookService.killKernel(userId, notebookId)
                if (error == "")
                    call.respond(HttpStatusCode.OK, "")
                else
                    call.respond(HttpStatusCode.InternalServerError, error)
            }
        }
    }
}