package org.appmat.polyglot.notebook.controller

import org.appmat.polyglot.notebook.Notebook
import org.appmat.polyglot.notebook.dto.*
import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType
import org.appmat.polyglot.notebook.kernel.PolyglotVariable
import org.appmat.polyglot.notebook.paragraph.Paragraph

object NotebookMapper {

    fun Notebook.toNotebookWithIdAndTitleDto() = NotebooksDto(notebookId = id, title = title)

    fun List<Notebook>.toNotebooksDto() : List<NotebooksDto> {
        val notebooksDto = arrayListOf<NotebooksDto>()
        this.forEach {
            notebooksDto.add(it.toNotebookWithIdAndTitleDto())
        }
        return notebooksDto
    }

    fun Notebook.toNotebookDto() = NotebookDto(id = id, title = title, date = date, paragraphs = paragraphs.toParagraphsDto())

    private fun List<Paragraph>.toParagraphsDto() : List<ParagraphDto> {
        val paragraphsDto = arrayListOf<ParagraphDto>()
        this.forEach {
            paragraphsDto.add(it.toParagraphDto())
        }
        return paragraphsDto
    }

    private fun Paragraph.toParagraphDto() = ParagraphDto(id = id, notebookId = notebookId, language = language, code = code, output = output)

    // -----

    private fun PolyglotVariable.toVariableDto() = PolyglotVariableDto(name = name, type = polyglotType?.toDtoType(), value = value.toString())

    private fun PolyglotType.toDtoType() : String {
        return when (this) {
            is PolyglotType.SimpleType.IntegerType -> PolyglotType.SimpleType.IntegerType().name
            is PolyglotType.SimpleType.StringType -> PolyglotType.SimpleType.StringType().name
            is PolyglotType.SimpleType.DoubleType -> PolyglotType.SimpleType.DoubleType().name
            is PolyglotType.SimpleType.LongType -> PolyglotType.SimpleType.LongType().name
            is PolyglotType.SimpleType.BooleanType -> PolyglotType.SimpleType.BooleanType().name

            is PolyglotType.ComplexType.ListType -> PolyglotType.ComplexType.ListType().name
            is PolyglotType.ComplexType.MapType -> PolyglotType.ComplexType.MapType().name
            is PolyglotType.ComplexType.SetType -> PolyglotType.ComplexType.SetType().name
        }
    }
    private fun List<PolyglotVariable>.toVariablesDto() : ArrayList<PolyglotVariableDto> {
        val variablesDto = arrayListOf<PolyglotVariableDto>()
        this.forEach {
            variablesDto.add(it.toVariableDto())
        }
        return variablesDto
    }

    fun PolyglotContext.toContextDto() =
        PolyglotContextDto(
            variables = variables.toVariablesDto(),
            constants = constants?.toVariablesDto() ?: arrayListOf<PolyglotVariableDto>(),
            objects = objects?.toVariablesDto() ?: arrayListOf<PolyglotVariableDto>()
        )
}