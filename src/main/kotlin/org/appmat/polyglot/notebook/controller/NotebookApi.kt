package org.appmat.polyglot.notebook.controller

import io.ktor.locations.*

@Location("/api")
class Api {

    @Location("/notebook")
    class NotebookApi(val parent: Api) {

        /**
         * Прогрузить все ноутбуки
         *
         */
        @Location("/all")
        class All(val parent: NotebookApi)

        /**
         * Создание ноутбука
         *
         * @param title название ноутбука
         */
        @Location("/")
        class Create(val parent: NotebookApi)

        /**
         * Метод удаления ноутбука
         *
         * @param notebookId Идентификатор ноутбука
         */
        @Location("/{notebookId}")
        data class Delete(val notebookId: String, val parent: NotebookApi)

        /**
         * Открыть заданный ноутбук
         *
         * @param notebookId Идентификатор ноутбука
         */
        @Location("/{notebookId}")
        data class Open(val notebookId: String, val parent: NotebookApi)

        /**
         * Метод обновления ноутбука
         *
         * @param notebookId
         */
        @Location("/{notebookId}")
        data class Update(val notebookId: String, val parent: NotebookApi)

        @Location("/{notebookId}/paragraph")
        data class Paragraph(val notebookId: String, val parent: NotebookApi) {

            /**
             * Создание параграфа
             *
             */
            @Location("/")
            data class Create(val parent: Paragraph)

            /**
             * Метод удаления параграфа
             *
             * @param paragraphId Идентификатор параграфа
             */
            @Location("/{paragraphId}")
            data class Delete(val paragraphId: String, val parent: Paragraph)

            /**
             * Запуск параграфа
             *
             * @param paragraphId Идентификатор параграфа
             */
            @Location("/{paragraphId}")
            data class Run(val paragraphId: String, val parent: Paragraph)
        }

        @Location("/{notebookId}/kernel")
        data class Kernel(val notebookId: String, val parent: NotebookApi) {
            /**
             * Запуск ядра приложения
             */
            @Location("/start")
            data class Start(val parent: Kernel)

            /**
             * Деинициализация ядра приложения
             */
            @Location("/kill")
            data class Kill(val parent: Kernel)
        }
    }
}