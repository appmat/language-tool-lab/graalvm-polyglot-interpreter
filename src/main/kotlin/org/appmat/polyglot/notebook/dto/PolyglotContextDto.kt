package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PolyglotContextDto(
    @SerialName(value = "variables") var variables: ArrayList<PolyglotVariableDto>,
    @SerialName(value = "constants") var constants: ArrayList<PolyglotVariableDto>,
    @SerialName(value = "objects") var objects: ArrayList<PolyglotVariableDto>,
)