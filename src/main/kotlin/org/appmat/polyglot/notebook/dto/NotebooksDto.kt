package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NotebooksDto(
    @SerialName(value = "id") val notebookId: Int,
    @SerialName(value = "title") val title: String
)