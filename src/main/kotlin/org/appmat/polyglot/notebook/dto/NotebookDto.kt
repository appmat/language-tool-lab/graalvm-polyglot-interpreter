package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NotebookDto(
    @SerialName(value = "id") val id: Int,
    @SerialName(value = "title") val title: String,
    @SerialName(value = "date") val date: String,
    @SerialName(value = "paragraphs") val paragraphs: List<ParagraphDto>
)