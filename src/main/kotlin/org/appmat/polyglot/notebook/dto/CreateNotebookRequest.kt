package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CreateNotebookRequest(
    @SerialName(value = "title") @Required val title: String,
)