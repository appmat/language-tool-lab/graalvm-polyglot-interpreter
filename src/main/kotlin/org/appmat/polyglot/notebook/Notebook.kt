package org.appmat.polyglot.notebook

import org.appmat.polyglot.notebook.paragraph.Paragraph

data class Notebook(
    var id: Int,
    var title: String,
    var date: String,
    var paragraphs: List<Paragraph> = emptyList()
)