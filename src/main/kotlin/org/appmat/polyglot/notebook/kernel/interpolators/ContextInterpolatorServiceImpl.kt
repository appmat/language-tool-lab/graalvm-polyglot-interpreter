package org.appmat.polyglot.notebook.kernel.interpolators

import org.appmat.polyglot.notebook.kernel.interpreters.Interpreters
import org.appmat.polyglot.notebook.kernel.interpreters.defaultShell
import org.appmat.polyglot.notebook.kernel.*
import java.util.*

class ContextInterpolatorServiceImpl: ContextInterpolatorService {

    private val notebookInterpreters: MutableMap<String, MutableMap<String, Interpreters>> = mutableMapOf()
    private val notebookLanguage: MutableMap<String, String> = mutableMapOf()
    private var interpreter: Interpreters = defaultShell
    private var interpolator: ContextInterpolate = defaultContextInterpolator
    private var ctx: PolyglotContext = defaultContext
    private val listOfInterpreters = listOf("kotlin", "java", "python", "scala", "javascript")

    override fun initInterpreters(notebookId: String): Optional<String> {
        val mapOfInterpreters: MutableMap<String, Interpreters> = mutableMapOf()
        try {
            listOfInterpreters.stream().forEach { interpreter ->
                val shell = mapOfShells.getValue(interpreter)
                shell.initShell()
                mapOfInterpreters[shell.language] = shell
                println("${shell.language} shell is inited")
            }
            notebookInterpreters[notebookId] = mapOfInterpreters
        } catch (ex: java.lang.Error) {
            return Optional.of(ex.message.toString())
        } catch (ex: Exception) {
            return Optional.of(ex.message.toString())
        } catch (ex: UnsatisfiedLinkError) {
            return Optional.of("Python JEP exception: ${ex.message}")
        } catch (ex: jep.JepException) {
            return Optional.of("Python JEP exception: ${ex.message}")
        } catch (ex: java.lang.NullPointerException) {
            return Optional.of(ex.message.toString())
        }
        return Optional.empty()
    }

    override fun shutdownInterpreters(notebookId: String): Optional<String> {
        val mapOfInterpreters = notebookInterpreters[notebookId]
        try {
            listOfInterpreters.stream().forEach { interpreter ->
                val shell = mapOfInterpreters?.get(interpreter)
                shell?.shutdown()
                println("${shell?.language} shell is shutdown")
            }
            notebookInterpreters[notebookId] = mutableMapOf()
        } catch (ex: java.lang.Error) {
            return Optional.of(ex.message.toString())
        } catch (ex: Exception) {
            return Optional.of(ex.message.toString())
        } catch (ex: UnsatisfiedLinkError) {
            return Optional.of("Python JEP exception: ${ex.message}")
        } catch (ex: jep.JepException) {
            return Optional.of("Python JEP exception: ${ex.message}")
        } catch (ex: java.lang.NullPointerException) {
            return Optional.of(ex.message.toString())
        }
        return Optional.empty()
    }

    override fun interpolate(notebookId: String, lang: String, code: String): Map<PolyglotContext?, Optional<String>> {
        try {
            val prevLanguage = notebookLanguage[notebookId] ?: ""
            val interpreters = notebookInterpreters[notebookId]
            if (prevLanguage != "") {
                interpreter = interpreters!!.getValue(lang)
                interpolator = mapOfInterpolators.getValue(lang)
                val genCode = interpolator.generateInterpolatedCode(ctx)
                interpreter.execute(genCode)
                val newCtx = interpreter.getVariables()
                ctx = ctx.removeDuplicates(newCtx)
            }

            interpreter = interpreters!!.getValue(lang)
            interpreter.execute(code)
            val newCtx = interpreter.getVariables()
            ctx = ctx.removeDuplicates(newCtx)
            notebookLanguage[notebookId] = lang
            interpreter.reset()
            return mapOf(ctx to Optional.empty())
        } catch (ex: java.lang.Error) {
            return mapOf(null to Optional.of(ex.message.toString()))
        } catch (ex: Exception) {
            return mapOf(null to Optional.of(ex.message.toString()))
        } catch (ex: UnsatisfiedLinkError) {
            return mapOf(null to Optional.of("Python JEP exception: ${ex.message}"))
        } catch (ex: jep.JepException) {
            return mapOf(null to Optional.of("Python JEP exception: ${ex.message}"))
        } catch (ex: java.lang.NullPointerException) {
            return mapOf(null to Optional.of(ex.message.toString()))
        }
    }
}