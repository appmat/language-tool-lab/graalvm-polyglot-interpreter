package org.appmat.polyglot.notebook.kernel.interpreters.javascript

import org.appmat.polyglot.notebook.kernel.Printer

fun main() {
    val jsShell = JavascriptShell()
    jsShell.initShell()

    val ctx = jsShell.getVariables()
    Printer.raw(shell = jsShell.language, ctx = ctx)
}
