package org.appmat.polyglot.notebook.kernel.interpreters.python

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType
import org.appmat.polyglot.notebook.kernel.PolyglotVariable
import jep.JepConfig
import jep.SharedInterpreter
import jep.python.*
import org.appmat.polyglot.notebook.kernel.interpreters.Interpreters
import org.appmat.polyglot.notebook.kernel.interpreters.ShellConfiguration
import org.appmat.polyglot.notebook.kernel.interpreters.parseSimpleType

class PythonJepShell(): Interpreters {

    override val language: String
        get() = "python"

    private lateinit var variables: ArrayList<PolyglotVariable>
    private lateinit var objects: ArrayList<PolyglotVariable>
    private lateinit var jep: SharedInterpreter

    override fun initShell(config: ShellConfiguration?) {
        val conf = JepConfig().redirectStdout(System.out)
        SharedInterpreter.setConfig(conf)
        jep = SharedInterpreter()
        variables = arrayListOf<PolyglotVariable>()
        objects = arrayListOf<PolyglotVariable>()
    }

    override fun parseVariableType(name: String): PolyglotType? {
        val value = jep.getValue(name as String?)
        val type = value.javaClass.toString()
        val varType = type.split(".").last()
        var tp = "String"
        var found = false

        val variableType = parseSimpleType(varType)
            ?: with (type) {
            val t: PolyglotType? = when (varType) {
                "ArrayList" -> {
                    try {
                        val str: String = (jep.getValue(name) as ArrayList<String>)[0]
                    } catch (ex: Exception) {
                        when (ex) {
                            is java.lang.ClassCastException,
                            is IndexOutOfBoundsException -> tp = "Long"
                        }
                    }
                    PolyglotType.ComplexType.ListType(type = parseSimpleType(tp))
                }
                "HashMap" -> {
                    var keyType = ""
                    var valueType = ""
                    try {
                        (jep.getValue(name) as Map<String, Long>).forEach{ (k,v) ->
                            val kk: String = k
                            val vv: Long = v
                        }
                        keyType = "String"
                        valueType = "Long"
                        found = true
                    } catch (ex: java.lang.ClassCastException) {
                        found = false
                    }
                    if (!found) {
                        try {
                            (jep.getValue(name) as Map<Long, String>).forEach { (k,v) ->
                                val kk: Long = k
                                val vv: String = v
                            }
                            keyType = "Long"
                            valueType = "String"
                            found = true
                        } catch (ex: java.lang.ClassCastException) {
                            found = false
                        }
                    }
                    if (!found) {
                        try {
                            (jep.getValue(name) as Map<String, String>).forEach { (k,v) ->
                                val kk: String = k
                                val vv: String = v
                            }
                            keyType = "String"
                            valueType = "String"
                            found = true
                        } catch (ex: java.lang.ClassCastException) {
                            found = false
                        }
                    }
                    if (!found) {
                        try {
                            (jep.getValue(name) as Map<Long, Long>).forEach { (k,v) ->
                                val kk: Long = k
                                val vv: Long = v
                            }
                            keyType = "Long"
                            valueType = "Long"
                            found = true
                        } catch (ex: java.lang.ClassCastException) {
                            found = false
                        }
                    }
                    PolyglotType.ComplexType.MapType(key = parseSimpleType(keyType), value = parseSimpleType(valueType))
                }
                "PyObject" -> {
                    tp = try {
                        val symbol = value.toString()[1]
                        if (symbol == '\'' || symbol == '\"')
                            "String"
                        else {
                            val element = value.toString().split("{")[1].split(", ")[0]
                            val el = Integer.parseInt(element)
                            "Int"
                        }
                    } catch (ex: Exception) {
                        "String"
                    }
                    PolyglotType.ComplexType.SetType(type = parseSimpleType(tp))
                }
                else -> null
            }
            return t
        }
        return variableType
    }

    override fun getVariables(): PolyglotContext {
        variables = arrayListOf<PolyglotVariable>()
        objects = arrayListOf<PolyglotVariable>()

        val secretVar: String = "all_variables"
        jep.exec("$secretVar = [item for item in dir() if not item.startswith(\"__\") and item != \"jep\"]")
        val variableList = jep.getValue("$secretVar", List::class.javaObjectType)

        for (name in variableList) {
            val value = jep.getValue(name as String?)
            val type = value.javaClass.toString()
            if (name == secretVar || name == "redirect_streams") continue
            val polyglotType = parseVariableType(name!!)

            when (value::class) {
                // set
                PyObject::class ->
                    objects.add(PolyglotVariable(name = name.toString(), polyglotType = polyglotType, value = value.toString(), type = type))
                else ->
                    variables.add(PolyglotVariable(name = name.toString(), polyglotType = polyglotType, value = value.toString(), type = type))
            }
        }
        return PolyglotContext(variables, null, objects)
    }

    override fun execute(code: String) {
        jep.exec(code)
    }

    override fun reset() {
    }

    override fun shutdown() {
        jep.close()
    }
}
