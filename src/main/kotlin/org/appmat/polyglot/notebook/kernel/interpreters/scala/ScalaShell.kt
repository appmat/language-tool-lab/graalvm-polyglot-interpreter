package org.appmat.polyglot.notebook.kernel.interpreters.scala

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType
import org.appmat.polyglot.notebook.kernel.PolyglotVariable
import org.apache.flink.api.scala.*
import org.appmat.polyglot.notebook.kernel.interpreters.Interpreters
import org.appmat.polyglot.notebook.kernel.interpreters.ShellConfiguration
import org.appmat.polyglot.notebook.kernel.interpreters.parseSimpleType
import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.reflect.internal.Scopes
import scala.reflect.internal.Symbols
import scala.reflect.internal.Trees
import scala.tools.nsc.interpreter.IMain
import scala.tools.nsc.interpreter.NamedParamClass
import java.io.PrintWriter
import scala.tools.nsc.Settings;
import scala.tools.nsc.interpreter.IMain.Request
import scala.tools.nsc.settings.MutableSettings;
import java.io.OutputStream
import java.lang.reflect.Field

data class RequestLine(var name: String, var value: String = "", var type: String)

class ScalaFlinkShell(): Interpreters {
    lateinit var shell: IMain
    private val env: ExecutionEnvironment = ExecutionEnvironment.createLocalEnvironment(1)
    lateinit var scopes: Scopes.Scope

    private lateinit var variables: ArrayList<PolyglotVariable>
    private lateinit var constants: ArrayList<PolyglotVariable>

    override val language: String
        get() = "scala"

    override fun initShell(config: ShellConfiguration?) {
        val settings = Settings()
        (settings.usejavacp() as MutableSettings.BooleanSetting).`value_$eq`(true)
        shell = IMain(settings, PrintWriter(OutputStream.nullOutputStream()))
        shell.bind(NamedParamClass("env", ExecutionEnvironment::class.java.name, env))
    }

    override fun getVariables(): PolyglotContext {
        variables = arrayListOf<PolyglotVariable>()
        constants = arrayListOf<PolyglotVariable>()

        var colons: List<Request>? = shell.prevRequestList()

        val mapOfNameValue: MutableMap<String, String> = mutableMapOf()
        val setNames: MutableSet<String> = mutableSetOf()
        val mapOfNameLineType: MutableMap<String, String> = mutableMapOf()
        val mapOfNameConst: MutableMap<String, Boolean> = mutableMapOf()

        if (colons != null) {
            for (colon in colons) {
                val reqLine = colon.loadAndRun()._1
                if (reqLine.isEmpty())
                    continue

                val ires = (colon.trees().last() as Trees.ValDef).rhs().toString().split(".")[0]
                val (name, value, lineType) = parseRequestLine(colon.loadAndRun()._1)

                if (name.contains("\$ires") && value != "()") {
                    mapOfNameValue[ires] = value
                } else {
                    mapOfNameValue[name.trimIndent()] = value
                    setNames.add(name.trimIndent())
                }
                mapOfNameLineType[name.trimIndent()] = lineType
                mapOfNameConst[name.trimIndent()] = colon.toString().contains("val".toRegex())
            }
        }

        setNames.forEach {
            shell.interpret(it, true)
        }

        colons = shell.prevRequestList()
        if (colons != null) {
            for (colon in colons) {
                val reqLine = colon.loadAndRun()._1
                if (reqLine.isEmpty())
                    continue

                val ires = (colon.trees().last() as Trees.ValDef).rhs().toString().split(".")[0]
                val (name, value, lineType) = parseRequestLine(colon.loadAndRun()._1)

                if (name.contains("\$ires") && value != "()") {
                    mapOfNameValue[ires] = value
                } else {
                    mapOfNameValue[name.trimIndent()] = value
                    setNames.add(name.trimIndent())
                }
                mapOfNameLineType[name.trimIndent()] = lineType
                mapOfNameConst[name.trimIndent()] = colon.toString().contains("val".toRegex())
            }
        }

        scopes = (shell.replScope() as Scopes.Scope)
        for (elem in scopes) {
            val name = (elem as Symbols.MethodSymbol).rawname().toString()
            val polyglotType =  parseVariableType(elem.typeSignature().resultType().toString(), mapOfNameLineType[name])
            val value = parseValue(polyglotType, mapOfNameValue[name])
            val type = elem.typeSignature().resultType().toString()
            val const = if (mapOfNameConst[name] == true) "const" else null
            if (name != "env" && type != "Unit" && !name.contains("\$ires")) {
                if (const == "const")
                    constants.add(PolyglotVariable(name, value, polyglotType, type, const))
                else
                    variables.add(PolyglotVariable(name, value, polyglotType, type, null))
            }
        }

        return PolyglotContext(variables, constants, null)
    }

    private fun parseRequestLine(line: String) : RequestLine {
        val (name, typeAndValue) = line.split(": ")
        val (type, value) = typeAndValue.split(" = ")
        return RequestLine(name.trimIndent(), value.trimIndent(), type.trimIndent())
    }

    override fun parseVariableType(type: String): PolyglotType? {
        TODO("Not yet implemented")
    }

    private fun parseVariableType(type: String, lineType: String?): PolyglotType? {
        if (lineType == null) return null
        val variableType = parseSimpleType(type)
            ?: with (type) {
                val t: PolyglotType? = when (type) {
                    "List" -> {
                        val simpleType = parseSimpleType(
                            lineType.split("List")[1]
                            .replace("[", "")
                            .replace("]", "")
                        )
                        PolyglotType.ComplexType.ListType(type = simpleType)
                    }
                    "scala.collection.immutable.Map" -> {
                        val kv = Regex("Map\\[(\\w+).(\\w+)]").find(lineType)!!
                        val (k, v) = kv.destructured
                        PolyglotType.ComplexType.MapType(key = parseSimpleType(k), value = parseSimpleType(v))
                    }
                    "scala.collection.immutable.Set" -> {
                        val simpleType = parseSimpleType(
                            lineType.split("Set")[1]
                            .replace("[", "")
                            .replace("]", "")
                        )
                        PolyglotType.ComplexType.SetType(type = simpleType)
                    }
                    else -> null
                }
                return t
            }
        return variableType
    }

    private fun parseValue(type: PolyglotType?, value: String?) : String? {
        val returnedValue = value ?: return null
        val transformedValue = when (type) {
            is PolyglotType.SimpleType.StringType -> {
                "\"${returnedValue.trimIndent()}\""
            }
            is PolyglotType.ComplexType.SetType -> {
                returnedValue.split("Set")[1]
                    .replace("(", "{")
                    .replace(")", "}")
            }
            is PolyglotType.ComplexType.ListType -> {
                returnedValue.split("List")[1]
                    .replace("(", "[")
                    .replace(")", "]")
            }
            is PolyglotType.ComplexType.MapType -> {
                // проверка типов
                returnedValue.split("Map")[1]
                    .replace("(", "{")
                    .replace(")", "}")
                    .replace(" -> ", "=")
            }
            else -> returnedValue.trimIndent()
        }
        return transformedValue.trimIndent()
    }

    private fun clearRequestLine(): Unit {
        var field: Field = IMain::class.java.getDeclaredField("prevRequests")
        field.isAccessible = true
        @Suppress("UNCHECKED_CAST")
        var prevRequests = (field.get(shell) as ListBuffer<Request>)
        prevRequests.clear()
    }

    override fun execute(code: String) {
        try {
            // важно выполнять каждую строку по отдельности!
            code.split("\n").forEach {
                shell.interpret(it.trimIndent(), true)
            }
        } catch (ex: Exception) {
            println("ex: $ex")
        }
    }

    override fun reset() {
        clearRequestLine()
    }

    override fun shutdown() {
        shell.reset()
        shell.close()
    }
}


