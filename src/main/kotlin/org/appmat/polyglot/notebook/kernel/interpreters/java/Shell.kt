package org.appmat.polyglot.notebook.kernel.interpreters.java

import org.appmat.polyglot.notebook.kernel.Printer

fun main() {
    val jshell = JavaJShell()
    jshell.initShell()

    val ctx = jshell.getVariables()
    Printer.prettify(shell = "java", ctx = ctx)
}