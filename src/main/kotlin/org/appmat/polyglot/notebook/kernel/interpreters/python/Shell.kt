package org.appmat.polyglot.notebook.kernel.interpreters.python

import org.appmat.polyglot.notebook.kernel.Printer

fun main() {
    val jep = PythonJepShell()
    jep.initShell()

    val ctx = jep.getVariables()
    Printer.raw(shell = "python", ctx = ctx)
}