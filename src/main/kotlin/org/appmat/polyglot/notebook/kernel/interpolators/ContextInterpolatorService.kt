package org.appmat.polyglot.notebook.kernel.interpolators

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import java.util.*

interface ContextInterpolatorService {
    fun initInterpreters(notebookId: String): Optional<String>
    fun shutdownInterpreters(notebookId: String): Optional<String>
    fun interpolate(notebookId: String, lang: String, code: String): Map<PolyglotContext?, Optional<String>>
}