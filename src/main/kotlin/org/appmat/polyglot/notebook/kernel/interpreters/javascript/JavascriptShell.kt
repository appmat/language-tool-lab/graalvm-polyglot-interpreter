package org.appmat.polyglot.notebook.kernel.interpreters.javascript

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType
import org.appmat.polyglot.notebook.kernel.PolyglotVariable
import org.appmat.polyglot.notebook.kernel.interpreters.Interpreters
import org.appmat.polyglot.notebook.kernel.interpreters.ShellConfiguration
import org.graalvm.polyglot.Context
import org.graalvm.polyglot.PolyglotException
import org.graalvm.polyglot.Value
import java.util.regex.Pattern

class JavascriptShell : Interpreters {
    private lateinit var js: Context
    private lateinit var variables: ArrayList<PolyglotVariable>
    private lateinit var constants: ArrayList<PolyglotVariable>
    
    override val language: String
        get() = "javascript"

    override fun initShell(config: ShellConfiguration?) {
        js = Context.newBuilder()
            .allowAllAccess(true)
            .option("engine.WarnInterpreterOnly", "false")
            .build()
    }

    override fun getVariables(): PolyglotContext {
        constants = arrayListOf<PolyglotVariable>()
        variables = arrayListOf<PolyglotVariable>()
        
        val jsCtx = js.getBindings("js")
        val memberKeys = jsCtx.memberKeys
        var isConst: Boolean
        var variable: PolyglotVariable

        for (key in memberKeys) {
            isConst = false
            val member = jsCtx.getMember(key)
            if (!(member as Value).hasArrayElements()) continue
            if ((member as Value).toString() == "[]") continue

            val type = when ("${member.metaObject}") {
                "number" ->
                    PolyglotType.SimpleType.LongType()
                "string" ->
                    PolyglotType.SimpleType.StringType()
                "boolean" ->
                    PolyglotType.SimpleType.BooleanType()
                "function Array() { [native code] }" -> {
                    val t = parseElementsType(member, PolyglotType.ComplexType.ListType(), "")
                    PolyglotType.ComplexType.ListType(type = t.first)
                }
                "function Set() { [native code] }" -> {
                    val t = parseElementsType(member, PolyglotType.ComplexType.SetType(), "")
                    PolyglotType.ComplexType.SetType(type = t.first)
                }
                "function Object() { [native code] }" -> {
                    val t = parseElementsType(member, PolyglotType.ComplexType.MapType(), "object")
                    PolyglotType.ComplexType.MapType(key = t.first, value = t.second)
                }
                "function Map() { [native code] }" -> {
                    val t = parseElementsType(member, PolyglotType.ComplexType.MapType(), "map")
                    PolyglotType.ComplexType.MapType(key = t.first, value = t.second)
                }
                else -> continue
            }

            when ("${member.metaObject}") {
                "number" -> {
                    try {
                        js.evaluate("$key += 1")
                        js.evaluate("$key -= 1")
                    } catch (pe: PolyglotException) {
                        isConst = true
                    }
                }
                "string" -> {
                    try {
                        val value = "${jsCtx.getMember(key)}"
                        js.evaluate( "$key = ' '")
                        js.evaluate("$key = '$value'")
                    } catch (pe: PolyglotException) {
                        isConst = true
                    }
                }
                "function Object() { [native code] }", "function Map() { [native code] }" -> {
                    try {
                        js.evaluate("$key = new Map();")
                    } catch (pe: PolyglotException) {
                        isConst = true
                    }
                }
                "function Set() { [native code] }" -> {
                    try {
                        js.evaluate("$key = new Set();")
                    } catch (pe: PolyglotException) {
                        isConst = true
                    }
                }
                "function Array() { [native code] }" -> {
                    try {
                        js.evaluate("$key = [];")
                    } catch (pe: PolyglotException) {
                        isConst = true
                    }
                }
                else -> {}
            }

            variable = PolyglotVariable(name = key, value = parseVariableValue(type, member), polyglotType = type, type = member.metaObject.toString(), meta = if (isConst) "const" else null)
            if (isConst) {
                constants.add(variable)
            } else {
                variables.add(variable)
            }

        }

        return PolyglotContext(variables, constants)
    }

    override fun parseVariableType(type: String): PolyglotType? {
        TODO("Not yet implemented")
    }

    private fun parseElementsType(member: Value,
                                  rawComplexType: PolyglotType.ComplexType,
                                  rawType: String): Pair<PolyglotType.SimpleType?, PolyglotType.SimpleType?>
    {
        val res = when (rawComplexType) {
            is PolyglotType.ComplexType.ListType -> {
                val element = (member as Value).getArrayElement(0)
                if (element.isString) {
                    Pair(PolyglotType.SimpleType.StringType(), null)
                } else if (element.isNumber) {
                    Pair(PolyglotType.SimpleType.IntegerType(), null)
                } else {
                    Pair(null, null)
                }
            }
            is PolyglotType.ComplexType.SetType -> {
                var raw = (member as Value).toString().split("{")[1]
                var element = raw.split(", ")[0]
                var t : PolyglotType.SimpleType = PolyglotType.SimpleType.StringType()
                try {
                    val parsedElement = Integer.parseInt(element)
                    t = PolyglotType.SimpleType.IntegerType()
                } catch (ex: Exception) {

                }
                Pair(t, null)
            }
            is PolyglotType.ComplexType.MapType -> {
                var k = ""
                var v = ""
                if (rawType == "object" || rawType == "") {
                    val matcher = Pattern.compile("\\{(?<key>.*?):\\s*.*?(?<value>\\w+).*").matcher((member as Value).toString())
                    matcher.find()
                    k = matcher.group("key").toString().trimIndent()
                    v = matcher.group("value").toString().trimIndent()
                } else {
                    val matcher = Pattern.compile("\\{(?<key>.*?) => \\s*.*?(?<value>\\w+).*").matcher((member as Value).toString())
                    matcher.find()
                    k = matcher.group("key").toString().trimIndent()
                    v = matcher.group("value").toString().trimIndent()
                }

                var keyType : PolyglotType.SimpleType = PolyglotType.SimpleType.StringType()
                var valueType : PolyglotType.SimpleType = PolyglotType.SimpleType.StringType()
                try {
                    val elem = Integer.parseInt(k.replace("\"", ""))
                    keyType = PolyglotType.SimpleType.IntegerType()
                } catch (ex: Exception) {

                }
                try {
                    val elem = Integer.parseInt(v.replace("\"", ""))
                    valueType = PolyglotType.SimpleType.IntegerType()
                } catch (ex: Exception) {

                }
                Pair(keyType, valueType)
            }
            else -> Pair(null, null)
        }
        return res
    }

    override fun execute(code: String) {
        js.evaluate(code)
    }

    override fun reset() {
    }

    override fun shutdown() {
    }

    private fun Context.evaluate (code: String) = js.eval("js", code)

    private fun parseVariableValue(type: PolyglotType?, member: Value): String {
        var value = member.toString()
        when (type) {
            is PolyglotType.ComplexType.ListType -> {
                value = member.toString()
                    .split(")")
                    .last()
                    .replace("{", "[")
                    .replace("}", "]")
                if (value.contains("\"")) {
                    value = value.replace("\"", "")
                }
            }
            is PolyglotType.ComplexType.SetType -> {
                value = member.toString()
                    .split(")")
                    .last()
                if (value.contains("\"")) {
                    value = value.replace("\"", "")
                }
            }
            is PolyglotType.ComplexType.MapType -> {
                value = if (member.toString().contains("Map")) {
                    member.toString()
                        .split(")")[1]
                        .replace(" => ", "=")
                        .replace("\"".toRegex(), "")
                } else {
                    member.toString()
                        .split(")")
                        .last()
                        .replace(": ", "=")
                        .replace("\"".toRegex(), "")
                }
            }
            else -> {}
        }
        return value
    }

}