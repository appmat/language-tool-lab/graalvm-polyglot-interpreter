package org.appmat.polyglot.notebook

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.interpolators.ContextInterpolatorService
import org.appmat.polyglot.notebook.repository.NotebookRepository

class NotebookServiceImpl(
    private val notebookRepository: NotebookRepository,
    private val contextInterpolatorService: ContextInterpolatorService
): NotebookService {

    private var runningNotebookInterpreters: MutableList<String> = mutableListOf()

    override fun getAllNotebooks(userId: String): List<Notebook> {
         return notebookRepository.getAllNotebooks(userId.toInt())
    }

    override fun getNotebookById(userId: String, notebookId: String): Notebook {
        return notebookRepository.getNotebookById(userId.toInt(), notebookId.toInt())
    }

    override fun updateNotebook(notebook: Notebook) {
        notebookRepository.updateNotebook(notebook)
    }

    override fun deleteNotebookById(userId: String, notebookId: String) : String {
        notebookRepository.deleteNotebook(notebookId.toInt())
        return notebookId
    }

    override fun createNotebook(userId: String, title: String) : Notebook {
        return notebookRepository.createNotebook(userId.toInt(), title)
    }

    // всё ок
    override fun runParagraph(userId: String, notebookId: String, lang: String, code: String): Map<PolyglotContext?, String> {
        val error = startKernel(userId, notebookId)
        if (error == "") {
            val res = contextInterpolatorService.interpolate(notebookId, lang, code)
            val errOpt = res.values.first()
            if (errOpt.isPresent) {
                return mapOf(null to errOpt.get())
            }
            val ctx = res.keys.first()
            return mapOf(ctx to "")
        } else {
            return mapOf(null to error)
        }
    }

    // всё ок
    override fun startKernel(userId: String, notebookId: String) : String {
        if (!runningNotebookInterpreters.contains(notebookId)) {
            val errOpt = contextInterpolatorService.initInterpreters(notebookId)
            if (errOpt.isPresent) {
                return errOpt.get()
            }
            runningNotebookInterpreters.add(notebookId)
        }
        return ""
    }

    // всё ок
    override fun killKernel(userId: String, notebookId: String): String {
        if (runningNotebookInterpreters.contains(notebookId)) {
            runningNotebookInterpreters.remove(notebookId)
            val errOpt = contextInterpolatorService.shutdownInterpreters(notebookId)
            if (errOpt.isPresent) {
                return errOpt.get()
            }
        }
        return ""
    }

}