package org.appmat.polyglot.notebook.repository

import java.util.concurrent.atomic.AtomicInteger

object Sequence {
    private val counter = AtomicInteger()
    fun nextValue(): Int {
        return counter.getAndIncrement()
    }
}