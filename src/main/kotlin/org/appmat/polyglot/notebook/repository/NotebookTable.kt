package org.appmat.polyglot.notebook.repository

import org.appmat.polyglot.user.repository.UserTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object NotebookTable : Table("notebook") {
    val id = integer("id").autoIncrement()
    val notebookId = integer("notebook_id").uniqueIndex()
    val userId = reference(
        "user_id",
        UserTable.userId,
        onDelete = ReferenceOption.SET_NULL,
        onUpdate = ReferenceOption.CASCADE
    )
    val title = varchar("title", 50)
    val date = varchar("date", 50)

    override val primaryKey = PrimaryKey(notebookId)
}