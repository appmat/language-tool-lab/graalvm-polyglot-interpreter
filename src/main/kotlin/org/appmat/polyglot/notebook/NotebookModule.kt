package org.appmat.polyglot.notebook

import org.appmat.polyglot.notebook.kernel.interpolators.ContextInterpolatorService
import org.appmat.polyglot.notebook.kernel.interpolators.ContextInterpolatorServiceImpl
import org.appmat.polyglot.notebook.controller.NotebookController
import org.appmat.polyglot.notebook.repository.NotebookRepository
import org.appmat.polyglot.plugins.routing.KtorController
import org.appmat.polyglot.user.UserService
import org.appmat.polyglot.user.UserServiceImpl
import org.koin.dsl.bind
import org.koin.dsl.module

val notebookModule = module {
    single { NotebookRepository() }
    single<UserService> { UserServiceImpl(get()) }

    single<ContextInterpolatorService>  { ContextInterpolatorServiceImpl() }
    single<NotebookService> { NotebookServiceImpl(get(), get()) }

    single { NotebookController(get(), get(), get()) } bind KtorController::class
}