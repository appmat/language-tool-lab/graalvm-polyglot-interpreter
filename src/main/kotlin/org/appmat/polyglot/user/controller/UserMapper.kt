package org.appmat.polyglot.user.controller

import org.appmat.polyglot.user.User
import org.appmat.polyglot.user.dto.KeycloakUserRequest

object UserMapper {

    fun KeycloakUserRequest.toUser() = User(
        id = 0,
        authenticated = authenticated,
        clientId = clientId,
        realm = realm,
        subject = subject,
        idToken = idToken,
        sessionId = sessionId
    )
}