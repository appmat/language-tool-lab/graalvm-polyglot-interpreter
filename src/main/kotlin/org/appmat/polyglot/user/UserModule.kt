package org.appmat.polyglot.user

import io.ktor.application.*
import org.appmat.polyglot.plugins.routing.KtorController
import org.appmat.polyglot.user.controller.UserController
import org.appmat.polyglot.user.repository.UserRepository
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single

val userModule = module {
    single<UserRepository>()
    single<UserService> { UserServiceImpl(get()) }

    single { get<Application>().environment }
    single { UserController(get(), get()) } bind KtorController::class
}
