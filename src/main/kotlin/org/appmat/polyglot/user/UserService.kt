package org.appmat.polyglot.user

interface UserService {
    fun getUser(id: Long): User?
    suspend fun getUserIdByToken(token: String): String?
    suspend fun registerUser(user: User)
    suspend fun unRegisterUser(user: User)
}