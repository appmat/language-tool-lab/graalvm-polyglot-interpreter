package org.appmat.polyglot.user.repository

import org.jetbrains.exposed.sql.Table

object UserTable : Table("user") {
    val id = integer("id").autoIncrement()
    val userId = integer("user_id")
    val authenticated = bool("authenticated")
    val clientId = varchar("client_id", 200)
    val realm = varchar("realm", 100)
    val subject = varchar("subject", 200)
    val idToken = varchar("id_token", 2000)
    val sessionId = varchar("session_id", 500)

    override val primaryKey = PrimaryKey(userId)
}