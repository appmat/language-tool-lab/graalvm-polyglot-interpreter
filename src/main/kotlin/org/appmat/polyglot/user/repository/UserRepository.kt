package org.appmat.polyglot.user.repository

import org.appmat.polyglot.DatabaseFactory.dbQuery
import org.appmat.polyglot.notebook.paragraph.repository.ParagraphTable
import org.appmat.polyglot.user.User
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class UserRepository {

    private fun resultRowToUser(row: ResultRow) = User(
        id = row[UserTable.id],
        authenticated = row[UserTable.authenticated],
        clientId = row[UserTable.clientId],
        realm = row[UserTable.realm],
        subject = row[UserTable.subject],
        idToken = row[UserTable.idToken],
        sessionId = row[UserTable.sessionId]
    )

    suspend fun registerUser(user: User) = dbQuery {
        val newUserStatement = UserTable.insert {
            it[authenticated] = user.authenticated
            it[clientId] = user.clientId
            it[realm] = user.realm
            it[idToken] = user.idToken
            it[subject] = user.subject
            it[sessionId] = user.sessionId
        }
        newUserStatement.resultedValues?.singleOrNull()?.let(::resultRowToUser)
    }

    fun unRegisterUser(user: User) {
        transaction {
            UserTable.deleteWhere { (UserTable.userId eq user.id) }
        }
    }

    suspend fun getUser(clientId: String): User? = dbQuery {
        UserTable.select { UserTable.clientId eq clientId }
            .map(::resultRowToUser)
            .singleOrNull()
    }

    suspend fun getUserId(tokenId: String): User? = dbQuery {
        UserTable.select { UserTable.idToken eq tokenId }
            .map(::resultRowToUser)
            .singleOrNull()
    }

    fun getUser(): User? = null
}