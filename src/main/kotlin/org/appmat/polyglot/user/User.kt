package org.appmat.polyglot.user

data class User(
    val id: Int,
    val authenticated: Boolean,
    val clientId: String,
    val realm: String,
    val subject: String,
    val idToken: String,
    val sessionId: String,
)