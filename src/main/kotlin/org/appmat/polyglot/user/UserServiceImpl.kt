package org.appmat.polyglot.user

import org.appmat.polyglot.user.repository.UserRepository

class UserServiceImpl(
    private val userRepository: UserRepository,
) : UserService {

    override fun getUser(id: Long): User? {
        return userRepository.getUser()
    }

    override suspend fun getUserIdByToken(token: String): String? {
        return userRepository.getUserId(token)?.clientId
    }

    override suspend fun registerUser(user: User) {
        userRepository.registerUser(user)
    }

    override suspend fun unRegisterUser(user: User) {
        userRepository.unRegisterUser(user)
    }

}