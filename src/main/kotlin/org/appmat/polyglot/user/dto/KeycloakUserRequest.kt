package org.appmat.polyglot.user.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class KeycloakUserRequest(
    @SerialName(value = "authenticated") val authenticated: Boolean,
    @SerialName(value = "clientId") val clientId: String,
    @SerialName(value = "realm") val realm: String,
    @SerialName(value = "subject") val subject: String,
    @SerialName(value = "idToken") val idToken: String,
    @SerialName(value = "sessionId") val sessionId: String,
)