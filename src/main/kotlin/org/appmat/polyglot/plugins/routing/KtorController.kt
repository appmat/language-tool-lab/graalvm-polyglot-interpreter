package org.appmat.polyglot.plugins.routing

import io.ktor.routing.*

interface KtorController {
    val routing: Routing.() -> Unit
}
