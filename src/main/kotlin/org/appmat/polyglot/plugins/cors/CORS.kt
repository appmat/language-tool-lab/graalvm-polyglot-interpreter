package org.appmat.polyglot.plugins.cors

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.http.HttpMethod

fun Application.configureCORS(additionalHost: List<String>) {
    install(CORS) {
        allowNonSimpleContentTypes = true
        additionalHost.forEach {
            host(it)
        }

        allowHeaders {
            true
        }
        anyHost()

        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
    }
}
