package org.appmat.polyglot.plugins.di

import io.ktor.application.*
import org.koin.core.module.Module
import org.koin.ktor.ext.Koin
import org.koin.logger.slf4jLogger

fun Application.configureDI(koinModules: List<Module>) {
    install(Koin) {
        slf4jLogger(level = org.koin.core.logger.Level.ERROR)
        modules(koinModules)
    }
}