package org.appmat.polyglot.plugins.monitoring

import io.ktor.locations.*

@Location("/metrics")
class Metrics
