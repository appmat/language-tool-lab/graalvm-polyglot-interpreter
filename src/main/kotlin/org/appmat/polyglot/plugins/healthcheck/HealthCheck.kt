package org.appmat.polyglot.plugins.healthcheck

import io.ktor.application.*
import ktor_health_check.Health

fun Application.configureHealthChecks() {
    install(Health) {
        // add custom readiness checks if needed
        /*
        readyCheck("database") {
            db.isAlive
        }*/
    }
}
